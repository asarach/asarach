<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ContactMail;
use Mail ;


class PageController extends Controller
{

    public function about()
    {
        return view('pages.about');
    }

    public function services()
    {
        return view('pages.services');
    }

    public function contact()
    {
        return view('pages.contact');
    }

    public function postContact(Request $request)
    {
        $data = $request->all();

        $result = Mail::to('support@asarach.com')->send(new ContactMail($data));
        return redirect('/');
    }
    
}
