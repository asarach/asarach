<?php

namespace App\Console\Commands;

use DB;
use Carbon\Carbon;
use DirectoryIterator;
use Illuminate\Console\Command;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\File;
use Stichoza\GoogleTranslate\GoogleTranslate;

class CleanAssets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clean:assets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $folders = [
            base_path('resources/views'), //1179
        ];
        $img_folders = base_path('public/assets/img');

        $assets = $this->getAssets($folders);
        // dd($assets);
        $assets = $this->deleteAssets($img_folders, $assets);
        
    }

    public function getAssets($folders)
    {
        $trans = [];
        foreach ($folders as $folder) {
            $dir = $folder;
            foreach (glob("$dir/*") as $file) {
                $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));

                if (in_array($ext, ['php', 'vue', 'js'])) {
                    $content = file_get_contents("$file");
                    preg_match_all('/asset\(["\'][^*?"<>|:]*["\']\)/', $content, $matches, 0);
                    try {
                        foreach ($matches[0] as $matche) {
                            if (strpos($matche, 'asset("') !== false) {
                                $exp = explode('"', $matche);
                            } else {
                                $exp = explode("'", $matche);
                            }
                            $trans[] =  $exp[1];
                        }
                    } catch (\Exception $e) {
                    }
                } else {
                    $trans = array_merge($trans, $this->getAssets([$file]));
                }
            }
        }
        return $trans;
    }
    public function deleteAssets($path, $assets)
    {


        foreach (glob("$path/*") as $file) {
            $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
            if (in_array($ext, ['jpg', 'png', 'svg'])) {
                $name = str_replace('C:\Wnmp\html\asarach\public/', '', $file );
                if (!in_array($name, $assets)) {
                    File::delete(public_path($name));
                }
            } else {
                $this->deleteAssets($file, $assets);
            }
        }
    }
}
