@extends('layouts.app')
@section('main')
<div role="main" class="main">

    <section class="page-header page-header-modern page-header-background bg-color-dark p-relative z-index-1 lazyload"
        data-bg-src="{{ asset('assets/img/bg/page-header-bg.jpg') }}">
        <span class="custom-circle custom-circle-1 bg-color-light custom-circle-blur appear-animation"
            data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="400"></span>
        <span class="custom-circle custom-circle-2 bg-color-primary appear-animation" data-appear-animation="zoomIn"
            data-appear-animation-delay="500"></span>
        <span class="custom-circle custom-circle-3 bg-color-primary appear-animation" data-appear-animation="zoomIn"
            data-appear-animation-delay="600"></span>
        <div class="container">
            <div class="row mt-5">
                <div class="col">
                    <ul class="breadcrumb breadcrumb-light custom-title-with-icon-primary d-block">
                        <li><a href="#">Home</a></li>
                        <li class="active">Our Insights</li>
                    </ul>
                    <h1 class="custom-text-10 font-weight-bolder">Our Insights</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="container py-5 p-relative z-index-2">
        <div class="row">
            <div class="col-lg-9">
                <div class="row">
                    <div class="col pb-5">
                        <article>
                            <p class="custom-font-tertiary text-uppercase custom-text-2 mb-1 appear-animation"
                                data-appear-animation="fadeInRightShorter" data-appear-animation-delay="200">FEBRUARY 4,
                                2021</p>
                            <h4 class="text-color-dark custom-text-8 font-weight-bolder mb-3 appear-animation"
                                data-appear-animation="fadeInRightShorter" data-appear-animation-delay="400"><a
                                    href="demo-digital-agency-2-our-blog-post.html"
                                    class="text-color-dark text-color-hover-primary">An Interview With John Paul Doe</a>
                            </h4>
                            <p class="custom-text-4 mb-2 appear-animation" data-appear-animation="fadeInRightShorter"
                                data-appear-animation-delay="600">Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit. Sed imperdiet libero id nisi euismod, sed porta est consectetur. dolor sit amet,
                                consectetur adipiscing elit. Sed imperdiet libero.</p>
                            <a href="demo-digital-agency-2-our-blog-post.html"
                                class="text-color-primary text-color-hover-secondary custom-text-4 font-weight-bolder text-decoration-underline appear-animation"
                                data-appear-animation="fadeInRightShorter" data-appear-animation-delay="800">Read
                                More...</a>
                        </article>
                    </div>
                </div>
                <div class="row">
                    <div class="col pb-5">
                        <article>
                            <p class="custom-font-tertiary text-uppercase custom-text-2 mb-1 appear-animation"
                                data-appear-animation="fadeInRightShorter" data-appear-animation-delay="200">FEBRUARY 4,
                                2021</p>
                            <h4 class="text-color-dark custom-text-8 font-weight-bolder mb-3 appear-animation"
                                data-appear-animation="fadeInRightShorter" data-appear-animation-delay="400"><a
                                    href="demo-digital-agency-2-our-blog-post.html"
                                    class="text-color-dark text-color-hover-primary">Building An E-Commerce Site With
                                    CMS</a></h4>
                            <p class="custom-text-4 mb-2 appear-animation" data-appear-animation="fadeInRightShorter"
                                data-appear-animation-delay="600">Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit. Sed imperdiet libero id nisi euismod, sed porta est consectetur. dolor sit amet,
                                consectetur adipiscing elit. Sed imperdiet libero.</p>
                            <a href="demo-digital-agency-2-our-blog-post.html"
                                class="text-color-primary text-color-hover-secondary custom-text-4 font-weight-bolder text-decoration-underline appear-animation"
                                data-appear-animation="fadeInRightShorter" data-appear-animation-delay="800">Read
                                More...</a>
                        </article>
                    </div>
                </div>
                <div class="row">
                    <div class="col pb-5">
                        <article>
                            <p class="custom-font-tertiary text-uppercase custom-text-2 mb-1 appear-animation"
                                data-appear-animation="fadeInRightShorter" data-appear-animation-delay="200">FEBRUARY 4,
                                2021</p>
                            <h4 class="text-color-dark custom-text-8 font-weight-bolder mb-3 appear-animation"
                                data-appear-animation="fadeInRightShorter" data-appear-animation-delay="400"><a
                                    href="demo-digital-agency-2-our-blog-post.html"
                                    class="text-color-dark text-color-hover-primary">How To Design Mobile Apps For
                                    Everyone</a></h4>
                            <p class="custom-text-4 mb-2 appear-animation" data-appear-animation="fadeInRightShorter"
                                data-appear-animation-delay="600">Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit. Sed imperdiet libero id nisi euismod, sed porta est consectetur. dolor sit amet,
                                consectetur adipiscing elit. Sed imperdiet libero.</p>
                            <a href="demo-digital-agency-2-our-blog-post.html"
                                class="text-color-primary text-color-hover-secondary custom-text-4 font-weight-bolder text-decoration-underline appear-animation"
                                data-appear-animation="fadeInRightShorter" data-appear-animation-delay="800">Read
                                More...</a>
                        </article>
                    </div>
                </div>
                <div class="row">
                    <div class="col pb-5">
                        <article>
                            <p class="custom-font-tertiary text-uppercase custom-text-2 mb-1 appear-animation"
                                data-appear-animation="fadeInRightShorter" data-appear-animation-delay="200">FEBRUARY 4,
                                2021</p>
                            <h4 class="text-color-dark custom-text-8 font-weight-bolder mb-3 appear-animation"
                                data-appear-animation="fadeInRightShorter" data-appear-animation-delay="400"><a
                                    href="demo-digital-agency-2-our-blog-post.html"
                                    class="text-color-dark text-color-hover-primary">An Interview With John Paul Doe</a>
                            </h4>
                            <p class="custom-text-4 mb-2 appear-animation" data-appear-animation="fadeInRightShorter"
                                data-appear-animation-delay="600">Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit. Sed imperdiet libero id nisi euismod, sed porta est consectetur. dolor sit amet,
                                consectetur adipiscing elit. Sed imperdiet libero.</p>
                            <a href="demo-digital-agency-2-our-blog-post.html"
                                class="text-color-primary text-color-hover-secondary custom-text-4 font-weight-bolder text-decoration-underline appear-animation"
                                data-appear-animation="fadeInRightShorter" data-appear-animation-delay="800">Read
                                More...</a>
                        </article>
                    </div>
                </div>
                <div class="row">
                    <div class="col pb-5">
                        <article>
                            <p class="custom-font-tertiary text-uppercase custom-text-2 mb-1 appear-animation"
                                data-appear-animation="fadeInRightShorter" data-appear-animation-delay="200">FEBRUARY 4,
                                2021</p>
                            <h4 class="text-color-dark custom-text-8 font-weight-bolder mb-3 appear-animation"
                                data-appear-animation="fadeInRightShorter" data-appear-animation-delay="400"><a
                                    href="demo-digital-agency-2-our-blog-post.html"
                                    class="text-color-dark text-color-hover-primary">Building An E-Commerce Site With
                                    CMS</a></h4>
                            <p class="custom-text-4 mb-2 appear-animation" data-appear-animation="fadeInRightShorter"
                                data-appear-animation-delay="600">Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit. Sed imperdiet libero id nisi euismod, sed porta est consectetur. dolor sit amet,
                                consectetur adipiscing elit. Sed imperdiet libero.</p>
                            <a href="demo-digital-agency-2-our-blog-post.html"
                                class="text-color-primary text-color-hover-secondary custom-text-4 font-weight-bolder text-decoration-underline appear-animation"
                                data-appear-animation="fadeInRightShorter" data-appear-animation-delay="800">Read
                                More...</a>
                        </article>
                    </div>
                </div>
                <div class="row">
                    <div class="col pb-5">
                        <article>
                            <p class="custom-font-tertiary text-uppercase custom-text-2 mb-1 appear-animation"
                                data-appear-animation="fadeInRightShorter" data-appear-animation-delay="200">FEBRUARY 4,
                                2021</p>
                            <h4 class="text-color-dark custom-text-8 font-weight-bolder mb-3 appear-animation"
                                data-appear-animation="fadeInRightShorter" data-appear-animation-delay="400"><a
                                    href="demo-digital-agency-2-our-blog-post.html"
                                    class="text-color-dark text-color-hover-primary">How To Design Mobile Apps For
                                    Everyone</a></h4>
                            <p class="custom-text-4 mb-2 appear-animation" data-appear-animation="fadeInRightShorter"
                                data-appear-animation-delay="600">Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit. Sed imperdiet libero id nisi euismod, sed porta est consectetur. dolor sit amet,
                                consectetur adipiscing elit. Sed imperdiet libero.</p>
                            <a href="demo-digital-agency-2-our-blog-post.html"
                                class="text-color-primary text-color-hover-secondary custom-text-4 font-weight-bolder text-decoration-underline appear-animation"
                                data-appear-animation="fadeInRightShorter" data-appear-animation-delay="800">Read
                                More...</a>
                        </article>
                    </div>
                </div>

                <ul class="pagination pagination-rounded pagination-lg justify-content-center my-5">
                    <li class="page-item"><a class="page-link" href="#"><i class="fas fa-angle-left"></i></a></li>
                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#"><i class="fas fa-angle-right"></i></a></li>
                </ul>
            </div>
            <div class="col-lg-3 pt-4 pt-lg-0">
                <aside class="sidebar">
                    <div class="px-3 mb-4">
                        <h3 class="text-color-dark text-capitalize font-weight-bold text-5 m-0 mb-3">About The Blog</h3>
                        <p class="m-0">Lorem ipsum dolor sit amet, conse elit porta. Vestibulum ante justo, volutpat
                            quis porta diam.</p>
                    </div>
                    <div class="py-1 clearfix">
                        <hr class="my-2">
                    </div>
                    <div class="px-3 mt-4">
                        <form action="page-search-results.html" method="get">
                            <div class="input-group mb-3 pb-1">
                                <input class="form-control box-shadow-none text-1 border-0 bg-color-grey"
                                    placeholder="Search..." name="s" id="s" type="text">
                                <span class="input-group-append">
                                    <button type="submit" class="btn bg-color-grey text-1 p-2"><i
                                            class="fas fa-search m-2"></i></button>
                                </span>
                            </div>
                        </form>
                    </div>
                    <div class="py-1 clearfix">
                        <hr class="my-2">
                    </div>
                    <div class="px-3 mt-4">
                        <h3 class="text-color-dark text-capitalize font-weight-bold text-5 m-0 mb-3">Recent Posts</h3>
                        <div class="pb-2 mb-1">
                            <a href="#"
                                class="text-color-default text-uppercase text-1 mb-0 d-block text-decoration-none">10
                                Jan 2021 <span class="opacity-3 d-inline-block px-2">|</span> 3 Comments <span
                                    class="opacity-3 d-inline-block px-2">|</span> John Doe</a>
                            <a href="#"
                                class="text-color-dark text-hover-primary font-weight-bold text-3 d-block pb-3 line-height-4">Lorem
                                ipsum dolor sit amet</a>
                            <a href="#"
                                class="text-color-default text-uppercase text-1 mb-0 d-block text-decoration-none">10
                                Jan 2021 <span class="opacity-3 d-inline-block px-2">|</span> 3 Comments <span
                                    class="opacity-3 d-inline-block px-2">|</span> John Doe</a>
                            <a href="#"
                                class="text-color-dark text-hover-primary font-weight-bold text-3 d-block pb-3 line-height-4">Consectetur
                                adipiscing elit</a>
                            <a href="#"
                                class="text-color-default text-uppercase text-1 mb-0 d-block text-decoration-none">10
                                Jan 2021 <span class="opacity-3 d-inline-block px-2">|</span> 3 Comments <span
                                    class="opacity-3 d-inline-block px-2">|</span> John Doe</a>
                            <a href="#"
                                class="text-color-dark text-hover-primary font-weight-bold text-3 d-block pb-3 line-height-4">Vivamus
                                sollicitudin nibh luctus</a>
                        </div>
                    </div>
                    <div class="py-1 clearfix">
                        <hr class="my-2">
                    </div>
                    <div class="px-3 mt-4">
                        <h3 class="text-color-dark text-capitalize font-weight-bold text-5 m-0 mb-3">Recent Comments
                        </h3>
                        <div class="pb-2 mb-1">
                            <a href="#"
                                class="text-color-default text-2 mb-0 d-block text-decoration-none line-height-4">Admin
                                on <strong class="text-color-dark text-hover-primary text-4">Vivamus
                                    sollicitudin</strong> <span class="text-uppercase text-1 d-block pt-1 pb-3">10 Jan
                                    2021</span></a>
                            <a href="#"
                                class="text-color-default text-2 mb-0 d-block text-decoration-none line-height-4">John
                                Doe on <strong class="text-color-dark text-hover-primary text-4">Lorem ipsum
                                    dolor</strong> <span class="text-uppercase text-1 d-block pt-1 pb-3">10 Jan
                                    2021</span></a>
                            <a href="#"
                                class="text-color-default text-2 mb-0 d-block text-decoration-none line-height-4">Admin
                                on <strong class="text-color-dark text-hover-primary text-4">Consectetur
                                    adipiscing</strong> <span class="text-uppercase text-1 d-block pt-1 pb-3">10 Jan
                                    2021</span></a>
                        </div>
                    </div>
                    <div class="py-1 clearfix">
                        <hr class="my-2">
                    </div>
                    <div class="px-3 mt-4">
                        <h3 class="text-color-dark text-capitalize font-weight-bold text-5 m-0">Categories</h3>
                        <ul class="nav nav-list flex-column mt-2 mb-0 p-relative right-9">
                            <li class="nav-item"><a class="nav-link bg-transparent border-0" href="#">Design (2)</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link bg-transparent border-0 active" href="#">Photos (4)</a>
                                <ul>
                                    <li class="nav-item"><a class="nav-link bg-transparent border-0"
                                            href="#">Animals</a></li>
                                    <li class="nav-item"><a class="nav-link bg-transparent border-0 active"
                                            href="#">Business</a></li>
                                    <li class="nav-item"><a class="nav-link bg-transparent border-0" href="#">Sports</a>
                                    </li>
                                    <li class="nav-item"><a class="nav-link bg-transparent border-0" href="#">People</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item"><a class="nav-link bg-transparent border-0" href="#">Videos (3)</a>
                            </li>
                            <li class="nav-item"><a class="nav-link bg-transparent border-0" href="#">Lifestyle (2)</a>
                            </li>
                            <li class="nav-item"><a class="nav-link bg-transparent border-0" href="#">Technology (1)</a>
                            </li>
                        </ul>
                    </div>
                </aside>
            </div>
        </div>
    </div>

    <section class="get-in-touch bg-color-after-secondary p-relative overflow-hidden">
        <span class="custom-circle custom-circle-1 bg-color-light appear-animation" data-appear-animation="zoomIn"
            data-appear-animation-delay="100"></span>
        <span class="custom-circle custom-circle-2 bg-color-light appear-animation" data-appear-animation="zoomIn"
            data-appear-animation-delay="100"></span>
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <p class="mb-2 text-color-tertiary custom-text-7 custom-title-with-icon custom-title-with-icon-light appear-animation"
                        data-appear-animation="fadeInRightShorter" data-appear-animation-delay="200">Let’s Get in Touch
                    </p>
                    <h4 class="text-color-light font-weight-bolder custom-text-10 appear-animation"
                        data-appear-animation="fadeInRightShorter" data-appear-animation-delay="400">
                        We’re interested in talking<br />
                        about your business.
                    </h4>
                </div>
                <div
                    class="col-lg-4 d-flex align-items-center justify-content-start justify-content-lg-end mt-5 mt-lg-0">
                    <a herf="#"
                        class="btn btn-outline custom-btn-outline btn-light border-white rounded-0 px-4 py-3 text-color-light text-color-hover-dark bg-color-hover-light custom-text-6 line-height-6 font-weight-semibold custom-btn-with-arrow appear-animation"
                        data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="600">Let’s Talk!</a>
                </div>
            </div>
        </div>
    </section>

</div>
@endsection