<!DOCTYPE html>
<html dir="rtl" class="light">

<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title>Asarach Digital Agency</title>

	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="Porto - Responsive HTML5 Template">
	<meta name="author" content="okler.net">

	<!-- Favicon -->
	<link rel="shortcut icon" href="{{ asset('assets/img/favicon.ico') }}" type="image/x-icon" />
	<link rel="apple-touch-icon" href="{{ asset('assets/img/apple-touch-icon.png') }}">

	<!-- Mobile Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">



	<link id="googleFonts"
		href="https://fonts.googleapis.com/css?family=family=Lora:400,400i,700,700i|Poppins:300,400,500,600,700,800,900&display=swap"
		rel="stylesheet" type="text/css">
		<link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=Almarai:wght@300;400;700;800&display=swap" rel="stylesheet">


		<!-- Vendor CSS -->
	<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/vendor/fontawesome-free/css/all.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/vendor/animate/animate.compat.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/vendor/simple-line-icons/css/simple-line-icons.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/vendor/owl.carousel/assets/owl.carousel.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/vendor/owl.carousel/assets/owl.theme.default.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/vendor/magnific-popup/magnific-popup.min.css') }}">

	<!-- Theme CSS -->
	<link rel="stylesheet" href="{{ asset('assets/css/rtl-theme.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/rtl-theme-elements.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/rtl-theme-blog.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/rtl-theme-shop.css') }}">



	<!-- Demo CSS -->
	<link rel="stylesheet" href="{{ asset('assets/css/demos/demo-digital-agency-2.css') }}">
	<!-- Skin CSS -->
	<link id="skinCSS" rel="stylesheet" href="{{ asset('assets/css/skins/skin-digital-agency-2.css') }}">

	<!-- Theme Custom CSS -->
	<link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">

	<!-- Head Libs -->
	<script src="{{ asset('assets/vendor/modernizr/modernizr.min.js') }}"></script>

</head>

<body>

	<div class="body">
		<header id="header" class="header-transparent header-effect-shrink"
			data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': false, 'stickyStartAt': 1, 'stickyHeaderContainerHeight': 100}">
			<div class="header-body border-top-0 bg-color-dark box-shadow-none">
				<div class="header-container container">
					<div class="header-row">
						<div class="header-column header-column-logo">
							<div class="header-row">
								<div class="header-logo">
									<a href="{{ route('index') }}">
										<img alt="Porto" width="139" height="59"
											src="{{ asset('assets/img/logos/logo-1.png') }}">
									</a>
								</div>
							</div>
						</div>
						<div class="header-column header-column-nav-menu justify-content-end w-100">
							<div class="header-row">
								<div
									class="header-nav header-nav-links header-nav-dropdowns-dark header-nav-light-text order-2 order-lg-1">
									<div
										class="header-nav-main header-nav-main-mobile-dark header-nav-main-square header-nav-main-dropdown-no-borders header-nav-main-effect-2 header-nav-main-sub-effect-1">
										<nav class="collapse">
											<ul class="nav nav-pills" id="mainNav">
												<li class="dropdown-primary">
													<a class="nav-link text-capitalize font-weight-semibold custom-text-3 active"
														href="{{ route('index') }}">
														{{ trans('text.home') }}
													</a>
												</li>
												<li class="dropdown-primary">
													<a class="nav-link text-capitalize font-weight-semibold custom-text-3"
														href="{{ route('page.about') }}">
														{{ trans('text.about-us') }}
													</a>
												</li>
												<li class="dropdown">
													<a class="nav-link dropdown-toggle text-capitalize font-weight-semibold custom-text-3"
														class="dropdown-toggle" href="{{ route('service.index') }}">
														{{ trans('text.our-services') }}
													</a>
													<ul class="dropdown-menu">
														<li>
															<a class="dropdown-item font-weight-normal"
																href="{{ route('service.index') }}">
																{{ trans('text.overview') }}
															</a>
														</li>
														<li>
															<a class="dropdown-item font-weight-normal"
																href="{{ route('service.show', ['slug' => 'demo-digital-agency-2-our-services-detail']) }}">
																{{ trans('text.UX-design') }}
															</a>
														</li>
														<li>
															<a class="dropdown-item font-weight-normal"
																href="{{ route('service.show', ['slug' => 'demo-digital-agency-2-our-services-detail']) }}">
																{{ trans('text.web-development') }}
															</a>
														</li>
														<li>
															<a class="dropdown-item font-weight-normal"
																href="{{ route('service.show', ['slug' => 'demo-digital-agency-2-our-services-detail']) }}">
																{{ trans('text.content-marketing') }}
															</a>
														</li>
														<li>
															<a class="dropdown-item font-weight-normal"
																href="{{ route('service.show', ['slug' => 'demo-digital-agency-2-our-services-detail']) }}">
																{{ trans('text.seo') }}
															</a>
														</li>
														<li>
															<a class="dropdown-item font-weight-normal"
																href="{{ route('service.show', ['slug' => 'demo-digital-agency-2-our-services-detail']) }}">
																{{ trans('text.social-media-ads') }}
															</a>
														</li>
														<li>
															<a class="dropdown-item font-weight-normal"
																href="{{ route('service.show', ['slug' => 'demo-digital-agency-2-our-services-detail']) }}">
																{{ trans('text.content-strategy') }}
															</a>
														</li>
													</ul>
												</li>
												<li class="dropdown-primary">
													<a class="nav-link text-capitalize font-weight-semibold custom-text-3"
														href="{{ route('portfolio.index') }}">
														{{ trans('text.our-work') }}
													</a>
												</li>
												<li class="dropdown-primary">
													<a class="nav-link text-capitalize font-weight-semibold custom-text-3"
														href="{{ route('page.contact') }}">
														{{ trans('text.contact-us') }}
													</a>
												</li>
											</ul>
										</nav>
									</div>
									<button class="btn header-btn-collapse-nav" data-toggle="collapse"
										data-target=".header-nav-main nav">
										<i class="fas fa-bars"></i>
									</button>
								</div>
							</div>
						</div>
						<div class="header-column justify-content-end d-none d-lg-flex">
							<div class="header-row">
								<ul
									class="header-social-icons social-icons social-icons-clean social-icons-icon-light social-icons-big d-lg-flex m-0 ml-lg-2">
									<li class="social-icons-instagram"><a href="http://www.instagram.com/"
											target="_blank" class="text-4" title="Instagram"><i
												class="fab fa-instagram"></i></a></li>
									<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank"
											class="text-4" title="Twitter"><i class="fab fa-twitter"></i></a></li>
									<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank"
											class="text-4" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>

		@yield('main')

		<footer id="footer" class="mt-0 py-5">
			<div class="container py-5">
				<div class="row justify-content-between">
					<div class="col-sm-12 col-lg-6 col-xl-6">
						<div class="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="100">
						</div>
					</div>
					<div class="col-sm-12 col-lg-6 col-xl-4 col-info-footer mt-4 mt-sm-5 mt-lg-0">
						<div class="row">
							<div class="col-md-6 mt-3 mt-md-0">
								<p class="mb-0 text-left text-lg-right text-4 font-weight-medium appear-animation"
									data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200">
									{{ trans('text.email') }}</p>
								<p class="mb-0 text-left text-lg-right text-4 font-weight-medium appear-animation"
									data-appear-animation="fadeInUpShorter" data-appear-animation-delay="250"><a
										href="{{ trans('text.phone') }}"
										class="text-color-default">{{ trans('text.phone') }}
									</a></p>
							</div>
						</div>
					</div>
				</div>
				<div class="row justify-content-between">
					<div class="col-sm-12 col-lg-7 col-xl-6 d-none d-sm-flex">
						<div class="nav-footer w-100 pt-5 mt-0 mt-lg-4">
							<div class="row justify-content-between">
								<div class="col-auto mr-auto">
									<div class="appear-animation" data-appear-animation="fadeInUpShorter"
										data-appear-animation-delay="200">
										<div class="footer-nav footer-nav-links">
											<nav>
												<ul class="nav" id="footerNav">
													<li>
														<a class="text-color-hover-primary font-weight-semibold custom-text-2 text-capitalize"
															href="{{ route('service.index') }}">
															{{ trans('text.our-services') }}
														</a>
													</li>
													<li>
														<a class="text-color-hover-primary font-weight-semibold custom-text-2 text-capitalize"
															href="{{ route('portfolio.index') }}">
															{{ trans('text.our-work') }}
														</a>
													</li>
													<li>
														<a class="text-color-hover-primary font-weight-semibold custom-text-2 text-capitalize"
															href="{{ route('page.contact') }}">
															{{ trans('text.contact-us') }}
														</a>
													</li>
												</ul>
											</nav>
										</div>
									</div>
								</div>
								<div class="col-auto">
									<div class="appear-animation" data-appear-animation="fadeIn"
										data-appear-animation-delay="100">
										<ul
											class="header-social-icons social-icons social-icons-clean social-icons-icon-light social-icons-big d-none d-lg-block m-0 p-relative bottom-10">
											<li class="social-icons-instagram"><a href="http://www.instagram.com/"
													target="_blank" class="text-4" title="Instagram"><i
														class="fab fa-instagram"></i></a></li>
											<li class="social-icons-twitter"><a href="http://www.twitter.com/"
													target="_blank" class="text-4" title="Twitter"><i
														class="fab fa-twitter"></i></a></li>
											<li class="social-icons-facebook"><a href="http://www.facebook.com/"
													target="_blank" class="text-4" title="Facebook"><i
														class="fab fa-facebook-f"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12 col-lg-5 col-xl-6">
						<div class="d-flex justify-content-end custom-footer-copywriting pt-5 mt-0 mt-lg-4">
							<p class="mb-0 text-left text-lg-right d-block w-100 appear-animation"
								data-appear-animation="fadeIn" data-appear-animation-delay="300">
								{{ trans('text.copyright') }}</p>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</div>

	<!-- Vendor -->
	<script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
	<script src="{{ asset('assets/vendor/jquery.appear/jquery.appear.min.js') }}"></script>
	<script src="{{ asset('assets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
	<script src="{{ asset('assets/vendor/jquery.cookie/jquery.cookie.min.js') }}"></script>
	<script src="{{ asset('assets/vendor/popper/umd/popper.min.js') }}"></script>
	<script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('assets/vendor/jquery.validation/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js') }}"></script>
	<script src="{{ asset('assets/vendor/jquery.gmap/jquery.gmap.min.js') }}"></script>
	<script src="{{ asset('assets/vendor/lazysizes/lazysizes.min.js') }}"></script>
	<script src="{{ asset('assets/vendor/isotope/jquery.isotope.min.js') }}"></script>
	<script src="{{ asset('assets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
	<script src="{{ asset('assets/vendor/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
	<script src="{{ asset('assets/vendor/vide/jquery.vide.min.js') }}"></script>
	<script src="{{ asset('assets/vendor/vivus/vivus.min.js') }}"></script>

	<!-- Theme Base, Components and Settings -->
	<script src="{{ asset('assets/js/theme.js') }}"></script>


	<!-- Current Page Vendor and Views -->


	<!-- Current Page Vendor and Views -->
	<script src="{{ asset('assets/js/views/view.contact.js') }}"></script>

	<!-- Theme Custom -->
	<script src="{{ asset('assets/js/custom.js') }}"></script>

	<!-- Theme Initialization Files -->
	<script src="{{ asset('assets/js/theme.init.js') }}"></script>





</body>

</html>