<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'home' => 'الرئيسية',
    'about-us' => 'من نحن',
    'our-services' => 'خدماتنا',
    'overview' => 'نظرة عامة',
    'UX-design' => 'تصميم UX',
    'web-development' => 'تطوير المواقع',
    'content-marketing' => 'تسويق المحتوى',
    'seo' => 'تحسين محرك البحث',
    'social-media-ads' => 'إعلانات وسائل التواصل الاجتماعي',
    'content-strategy' => 'استراتيجية المحتوى',
    'our-work' => 'اعملنا',
    'contact-us' => 'اتصل بنا',
    'copyright' => 'كل الحقوق محفوظة',
    'email' => 'asaraach@gmail.com',
    'phone' => '+212662633509',
    'home-intro-h1' => 'تجربة التصميم والتسويق الذكي.',
    'home-intro-h2' => 'مرحبًا بكم في عصر التحول الرقمي',
    'home-intro-a' => 'البدء',
    'home-cards-h4-1' => 'تطوير المواقع',
    'home-cards-h4-2' => 'صتاعة المحتوى',
    'home-cards-h4-3' => 'تسويق المحتوى',
    'home-cards-p-1' => 'نصمم ونجري تجارب لتحسين نمو مؤسستك وتحسينه.',
    'home-cards-p-2' => 'إذا كنت تريد التأكد من أن عملك يعمل بشكل جيد ، فأنت بحاجة إلى جعل المحتوى الخاص بك هو الأفضل',
    'home-cards-p-3' => 'استفد من جميع موارد وتقنيات التسويق الأكثر قيمة',
    'view-all' => 'الكل',
    'home-services-h4' => 'يمكننا مساعدتك في',
    'home-services-text' => '',
    'get-in-touch-p' => 'لنتواصل معك',
    'get-in-touch-h4' => 'نحن مهتمون بالحديث <br /> عن نشاطك التجاري.',
    'get-in-touch-a' => 'لنتحدث!',
    'visite-website' => 'زيارة الموقع',
    'failed' => '',
    'failed' => '',

];
