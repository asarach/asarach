<?php

use Illuminate\Support\Facades\Route;
use Spatie\Honeypot\ProtectAgainstSpam;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', array('as' => 'index', 'uses' => 'HomeController@index'));
Route::get('/about', array('as' => 'page.about', 'uses' => 'PageController@about'));

Route::get('/contact', array('as' => 'page.contact', 'uses' => 'PageController@contact'));
Route::post('/contact', array('as' => 'store.contact', 'uses' => 'PageController@postContact'))->middleware(ProtectAgainstSpam::class);

Route::get('/services', array('as' => 'service.index', 'uses' => 'ServiceController@index'));
Route::get('/service/{slug}', array('as' => 'service.show', 'uses' => 'ServiceController@show'));

Route::get('/portfolio', array('as' => 'portfolio.index', 'uses' => 'PortfolioController@index'));
Route::get('/portfolio/{slug}', array('as' => 'portfolio.show', 'uses' => 'PortfolioController@show'));

Route::get('/blog', array('as' => 'post.index', 'uses' => 'PostController@index'));
Route::get('/post/{slug}', array('as' => 'post.show', 'uses' => 'PostController@show'));

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
